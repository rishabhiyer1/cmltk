#ifndef CMLTK
#define CMLTK

//#define DEBUG


#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <sys/stat.h>
#include <sys/types.h>
#include <algorithm>

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
#include "time.h"
#include <stdarg.h>
#include <assert.h>

#include "representation/cmltkRepresentation.h"
#include "optimization/contFunctions/contFunctions.h"
#include "optimization/contAlgorithms/contAlgorithms.h"
#include "machinelearning/machineLearning.h"
#include "utils/cmltkUtils.h"
#endif
