#ifndef CMLTK_CLASSIFIERS
#define CMLTK_CLASSIFIERS

#include "Classifiers.h"
#include "logisticRegression/L1LogisticRegression.h"
#include "logisticRegression/L2LogisticRegression.h"
#include "SVM/L1SmoothSVM.h"
#include "SVM/L2SmoothSVM.h"
#include "SVM/L2HingeSVM.h"

#endif
