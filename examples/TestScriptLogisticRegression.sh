#!/bin/bash

set -x

TEST_TYPE=0 #0:L2 Logistic Regression primal, 
train_exe="/homes/kaiwei/Downloads/liblinear-1.94/train"
cmltk_train_exe="/homes/kaiwei/transitory/CMLTK_development/CMTK/build/TestL2LogisticLossArgs"
C_Val=1 # regularization parameter
epsilon=0.0001
#data_set="/homes/kaiwei/Downloads/liblinear-1.94/heart_scale"
#data_set="/homes/kaiwei/Downloads/liblinear-1.94/a1a"
data_set="/n/rest/s0/kaiwei/news20.binary"
#data_set="/homes/kaiwei/Downloads/20news-bydate/matlab/lib_linear_data_format_withLabels_TFIDF_normalized_noID"



time $train_exe  -s $TEST_TYPE -c $C_Val -e $epsilon $data_set




algtype=5 #

awk '{print $1}' $data_set > ${data_set}.label

awk '{$1=""; print}' $data_set |  sed 's/:/ /g' > ${data_set}.feat
#N_Lines=`less ${data_set}.label | wc -l`
#printf '%s\n' "$N_Lines 1000001" | cat - ${data_set}.feat > ${data_set}.feat.wHeader

time $cmltk_train_exe -featureFile ${data_set}.feat -labelFile ${data_set}.label -Regularization 1 -MaxIter 100 -Epsilon 0.05 -algtype $algtype 







