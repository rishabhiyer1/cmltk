----------------- README ------------------------

CMLTK: A toolkit with API support for Convex Optimization and Machine Learning

Contributors:
- Rishabh Iyer
- John Halloran
- Kai Wei
- Anurag Sahoo

Features Supported

1) Convex Function API
 - Base class for convex optimization
 - List of Convex Functions Implemented: 
   L1LogistocLoss and L2LogistocLoss, 
   L1SmoothSVMLoss and L2SmoothSVMLoss, 
   L1HingeSVMLoss and L2HingeSVMLoss, 
   L1ProbitLossLoss and L2ProbitLoss, 
   L1HuberSVMLoss and L2HuberSVMLoss, 
   L1SmoothSVRLoss and L2SmoothSVRLoss, 
   L1HingeSVMLoss and L2HingeSVMLoss

2) Convex Optimization Algorithms API
 - Trust Region Newton (TRON)
 - LBFGS Algorithm
 - LBFGS OWL (L1 regularization)
 - Gradient Descent
 - Gradient Descent with Line Search
 - Gradient Descent with Nesterov's algorithm
 - Gradient Descent with Barzilai-Borwein step size
 - Stochastic Gradient Descent
 - Stochastic Gradient Descent with AdaGrad
 - Stochastic Gradient Descent with Dual Averaging
 - Stochastic Gradient Descent with Decaying Learning Rate
 
3) ML Classification API 
 - L1 Logistic Regression, 
 - L2 Logistic Regression
 - L1 Smooth SVM 
 - L2 Smooth SVM
 - L2 Smooth SVM
 
4) ML Regression API 
 - L1 Linear Regression
 - L2 Linear Regression
 - L1 Smooth SVRs 
 - L2 Smooth SVRs
 - L2 Hinge SVRs
 

